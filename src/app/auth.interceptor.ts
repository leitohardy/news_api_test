import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { environment } from '../environments/environment';

export class AuthInterceptor implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler) {

        const clonedRequest = req.clone({
            headers: req.headers.set('X-Api-Key', environment.apikey) 
        })

        return next.handle(clonedRequest);
    }
}