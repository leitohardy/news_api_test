import { Component, OnInit, ElementRef, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { filter, map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CountriesService } from 'src/app/services/countries.service';
import { CategoriesService } from 'src/app/services/categories.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy {

  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  @Output('searchEmitter') searchEmitter = new EventEmitter<string>();
  @Output('applyEmitter') applyEmitter = new EventEmitter<string>();
  
  subscriptions: Subscription[] = [];

  countries = [];
  categories = [];
  selectedCountryId;
  selectedCategoryId;

  constructor(
    private api: ApiService,
    private countriesService: CountriesService,
    private categoriesService: CategoriesService) { }

  ngOnInit() {

    this.selectedCountryId = environment.countryDefault;
    this.selectedCategoryId = environment.categoryDefault;

    this.countries = this.countriesService.getCountries();
    this.categories = this.categoriesService.getCategories();
    
    const subscription = fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((evt: any) => evt.target.value),
        // text length must be > 2 chars
        filter(res => res.length > 2),
        // emit after 0.5s of silence
        debounceTime(500),
        // emit only if data changes since the last emit
        distinctUntilChanged()
      )
      .subscribe((text: string) => this.submit(text));

      this.subscriptions.push(subscription);

  }

  submit(text) {
    this.searchEmitter.emit(text);
  }

  countryChange(event) {
    if (event && event.id) {
      this.selectedCountryId = event.id;
      this.api.setCountryParam(event.id);
    } else {
      this.selectedCountryId = environment.countryDefault;
      this.api.setCountryParam(environment.countryDefault);
    }

    this.applyEmitter.emit();
  }

  categoryChange(event) {

    if (event && event.id) {
      this.selectedCategoryId = event.id;
      this.api.setCategoryParam(event.id);
    } else {
      this.selectedCategoryId = environment.categoryDefault;
      this.api.setCategoryParam(environment.categoryDefault);
    }

    this.applyEmitter.emit();
  }

  reset() {
    this.searchInput.nativeElement.value = "";
    this.selectedCountryId = environment.countryDefault;
    this.api.setCountryParam(environment.countryDefault);
    this.selectedCategoryId = environment.categoryDefault;
    this.api.setCategoryParam(environment.categoryDefault);
    
    this.applyEmitter.emit();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
