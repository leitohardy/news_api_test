import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from "../../services/api.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit, OnDestroy {

  hasNews: boolean = false;
  subscriptions: Subscription[] = [];

  news;

  totalItems = 0;
  currentPage = 1;

  constructor(private api: ApiService) { }

  ngOnInit() {
    let subscription = this.loadData().subscribe(data => {
      this.totalItems = data['totalResults'];
      this.news = data;
      this.hasNews = true;
    });

    this.subscriptions.push(subscription);
  }

  // by Emmiter
  applyFilter(query = null) {

    let subscription;

    if (query !== null) {
      subscription = this.api.getData(query).subscribe(data => {
        this.totalItems = data['totalResults'];
        this.news = data;
      });
    } else {
      subscription = this.loadData().subscribe(data => {
        this.totalItems = data['totalResults'];
        this.news = data;
      });
    }

    this.subscriptions.push(subscription);
  }

  pageChange(event: any): void {
    this.currentPage = event.page;
    this.api.setPageParam(this.currentPage);

    let subscription = this.loadData().subscribe(data => {
      this.totalItems = data['totalResults'];
      this.news = data;
    });

    this.subscriptions.push(subscription);
  }

  loadData() {
    return this.api.getData();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
