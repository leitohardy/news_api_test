import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CategoriesService {
    public categories = [
        {name: 'Business', id: 'business'},
        {name: 'Entertainment', id: 'entertainment'},
        {name: 'General', id: 'general'},
        {name: 'Health', id: 'health'},
        {name: 'Science', id: 'science'},
        {name: 'Sports', id: 'sports'},
        {name: 'Technology', id: 'technology'},
    ];
    getCategories() {
        return this.categories;
    }

    getCategory(id) {

        let category = this.categories.filter(data => {
            return data.id === id;
        });

        return category[0]['name'] ? category[0]['name'] : 'not found';
    }
}