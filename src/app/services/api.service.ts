import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CountriesService } from './countries.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  reqUrl: string;
  
  public country: string = 'us';
  public category: string = 'general';

  // Use this to page through the results if the total results found is greater than the page size.
  public page: string = '1';

  // The number of results to return per page (request). 20 is the default, 100 is the maximum.
  public pageSize: string = '10';

  // News API has 2 main endpoints 'top-headlines' or 'everything'
  public endpoint: string = 'top-headlines';

  constructor(private http: HttpClient, private countriesService: CountriesService) {
    this.reqUrl = `http://newsapi.org/v2/${this.endpoint}`;
  }
  
  getData(query = null) {
    let params = this.setAllParams(query);
    return this.http.get(this.reqUrl, {params});
  }

  setAllParams(query = null) {

    let paramsMap = new Map<any,any>();
    paramsMap.set('pageSize', this.pageSize)
    paramsMap.set('page', this.page)
    paramsMap.set('country', this.country)
    paramsMap.set('category', this.category)

    if (query) {
      paramsMap.set('q', query)
    }

    let params = new HttpParams();
    paramsMap.forEach((value: any, key: any) => {
      params = params.set(key, value);
    });

    return params;
  }

  setPageParam(page) {
    this.page = page;
    this.getData();
  }

  setPageSizeParam(pageSize) {
    this.pageSize = pageSize;
  }
  
  setCountryParam(country) {
    this.country = country;
  }

  setCategoryParam(category) {
    this.category = category;
  }
}
