
# This is a simple test project. Which displays news using api and filter

Project use:
```
- https://newsapi.org/docs/endpoints/top-headlines
- Angular Universal
- RxJS
- ngx-bootstrap
- ng-select
```

Before use:
```sh
$ npm install
```

For compile project use this command:
```sh
$ npm run build:ssr && npm run serve:ssr
or
$ ng serve -o
```